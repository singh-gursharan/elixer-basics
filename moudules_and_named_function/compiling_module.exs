defmodule Times do
    def double(n) do
        n*2
    end 
end
IO.puts Times.double(4)
IO.puts Tiems.double(123)

# we could even write it as
# but it is not recommended to write it in this form
defmodule Times, do: (def double(n), do: n*2)

# this can be the recommended way
defmodule Times do
    def double(n), do: n*2
end