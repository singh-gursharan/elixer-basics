defmodule Greeter do
    def for(name, greeting) do
        fn
            (^name) -> "#{greeting} #{name}"
            (_) -> "I don't know you"
        end
    end
end

user = Greeter.for("ram", "hello")
IO.puts user.("ram")
IO.puts user.("sham")