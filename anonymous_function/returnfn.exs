fun1 = fn -> fn -> "hello" end end
ans = fun1.()
IO.inspect ans
ans = ans.()
IO.puts ans

# passing function as arguments
times_2 = fn n -> n*2 end
apply = fun (fun, value) -> fun.value end
ans = apply.(times_2,6)
IO.puts ans