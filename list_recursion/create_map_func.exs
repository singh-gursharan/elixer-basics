def map([], _func), do: []
def map([ head | tail ], func), do: [ func.(head) | map(tail, func) ]
list = MyList.map [1,2,3,4], fn (n) -> n*n end
IO.inspect list