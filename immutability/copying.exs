# everything is immutable in elixir

list1 = [3, 2, 1]
list2 = [4 | list1]
IO.inspect list2