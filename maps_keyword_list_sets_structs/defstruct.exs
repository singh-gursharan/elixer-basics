defmodule Subscriber do
defstruct name: "", paid: false, over_18: true
end

defmodule Main do
    def main do
        s1 = %Subscriber{}
        IO.inspect s1
        s2 = %Subscriber{name: "Dave"}
        IO.inspect s2
        s3 = %Subscriber{name: "Mary", paid: true}
        IO.inspect s3
    end
end

Main.main()

