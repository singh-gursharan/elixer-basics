defmodule Customer do
defstruct name: "", company: ""
end
defmodule BugReport do
defstruct owner: %Customer{}, details: "", severity: 1
end

defmodule Main do
    @report  %BugReport{owner: %Customer{name: "Dave", company: "Pragmatic"},details: "broken"}
    def main do
        IO.puts @report.owner.company
    end
    def put do
        # elixir has set of nested dictionary-access functions. One of these, put_in.
        put_in(@report.owner.company, "PragProg")
    end
    def update do
        # The update_in function lets us apply a function to a value in a structure
        update_in(@report.owner.name, &("Mr. " <> &1))
    end
end
IO.inspect Main.put
IO.inspect Main.update
IO.inspect Main.main
